## Hillel course Python advanced - Home work 3

### Requirements:
```
to create 3 html web pages with menu between pages, same footer on every page and following pages structure:
index.html - main page with table
about.html - page with picture and formatted text
contact.html - page with web form
style.css - all pages formats should be created using CSS in separate file style.css
```

### Clone my home work project from Gitlab:

    git clone git@gitlab.com:marchenko.alex/hillel_home_work_3.git
